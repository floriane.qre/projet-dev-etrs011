import json
from flask import Flask, render_template, request, url_for, redirect, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, logout_user
import functions
 

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite"
app.config["SECRET_KEY"] = "etrs011"
db = SQLAlchemy()
 
login_manager = LoginManager()
login_manager.init_app(app)
 
 
class Users(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(250), unique=True, nullable=False)
    password = db.Column(db.String(250), nullable=False)
 
 
db.init_app(app)
 
 
with app.app_context():
    db.create_all()
 
 
@login_manager.user_loader
def loader_user(user_id):
    return Users.query.get(user_id)
 
 
@app.route('/register', methods=["GET", "POST"])
def register():
    if request.method == "POST":
        user = Users(username=request.form.get("username"),
                     password=request.form.get("password"))
        db.session.add(user)
        db.session.commit()
        return redirect(url_for("login"))
    return render_template("signup.html")
 
 
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        user = Users.query.filter_by(
            username=request.form.get("username")).first()
        if user.password == request.form.get("password"):
            login_user(user)
            return redirect(url_for("index"))
    return render_template("login.html")
 
 
@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("profile"))

@app.route("/equipement")
def equipement():
    return render_template("equipement.html")

@app.route("/user")
def user():
    return render_template("user.html")

@app.route("/report")
def report():
    return render_template("report.html")
 
@app.route("/")
def profile():
    return render_template("login.html")

@app.route("/index")
def index():
    return render_template("index.html")

@app.route('/submitForm', methods=['POST'])
def submitForm():
    # Access form data using the 'request.form' attribute
    data = request.form.to_dict()
    new_key = data.get("nom")  # Utilisez "nom" comme clé unique

    with open('data.json', 'r') as file:
        existing_data = json.load(file)

    # ça filtre uniquement sur la clé "new_key" pour ajouter les données
    existing_data[new_key] = data

    with open('data.json', 'w') as file:
        json.dump(existing_data, file, indent=5)

    return jsonify({"message": "Form data saved successfully."}), 200

@app.route('/get_data')
def get_data():
    with open('data.json', 'r') as file:
        data = json.load(file)
        return jsonify(data)
    
@app.route('/submit_machine', methods=['POST'])
def submit_machine():
    data = request.form.to_dict()
    new_key = data.get("nom")

    # Appel de la fonction SNMP
    ip_address = data.get("ip")
    oid_to_walk = '1.3.6.1.2.1.2.2.1.5'
    snmp_results = functions.walk(ip_address, oid_to_walk)

    # Enregistrement des résultats SNMP dans un fichier JSON distinct lié à la machine
    snmp_filename = f"snmp_results_{new_key.replace('.', '_')}.json"
    with open(snmp_filename, 'w') as json_file:
        json.dump(snmp_results, json_file, indent=5)

    # Ajout des données du formulaire à la base de données (fichier JSON)
    database[new_key] = {
        'data': data,
        'snmp_results': snmp_filename
    }
    save_data_to_json()

    return jsonify({"message": "Form data and SNMP results saved successfully."}, snmp_results=snmp_results), 200

# Modifiez la route pour superviser une machine
@app.route('/supervise_function/<ip_address>', methods=['GET'])
def supervise_function(ip_address):
    # Ajoutez ici le code pour la fonctionnalité "Superviser" avec l'adresse IP
    # Utilisez la variable ip_address pour accéder à l'adresse IP de la machine à superviser
    oid_to_walk = '1.3.6.1.2.1.2.2.1.5'  # Spécifiez l'OID que vous souhaitez utiliser
    snmp_results = functions.walk(ip_address, oid_to_walk)  # Utilisez la fonction walk depuis functions.py

    return jsonify({"result": "Supervision successful."})

@app.route('/supprimer_machine', methods=['POST'])
def supprimer_machine():
    
    machine_id = request.form['nom']

    with open('data.json', 'r') as json_file:
        machines = json.load(json_file)
        if machine_id in machines:
            del machines[machine_id]

    with open('data.json', 'w') as json_file:
        json.dump(machines, json_file, indent=4)

    return jsonify({'success': True})
 
 
if __name__ == "__main__":
    app.run()
