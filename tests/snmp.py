import sys
from pysnmp.hlapi import *

def walk(host, oid):
    for (errorIndication,errorStatus,errorIndex,varBinds) in nextCmd(SnmpEngine(), 
        CommunityData('passprojet'), UdpTransportTarget((host, 161)), ContextData(), 
        ObjectType(ObjectIdentity(oid)), lexicographicMode=False):
        if errorIndication:
            print(errorIndication, file=sys.stderr)
            break
        elif errorStatus:
            print('%s at %s' % (errorStatus.prettyPrint(),
                                errorIndex and varBinds[int(errorIndex) - 1][0] or '?'), 
                                file=sys.stderr)             
            break
        else:
            for varBind in varBinds:
                print(varBind)

walk('192.168.176.2','1.3.6.1.2.1.2.2.1.5')

# def get_interfaces(target_host, community):
#     target_host = "192.168.176.2"
#     community = "passprojet"
#     error = None
#     oid = ".1.3.6.1.2.1.2.2.1.2."
#     oid_nb_int = ".1.3.6.1.2.1.2.1.0"
    
#     se = SnmpEngine()
#     cv2 = CommunityData(community, mpModel=1) #SNMPv2c
#     t1 = UdpTransportTarget((target_host, 161))
    
#     #Récupérer nombre d'interface
#     oi = ObjectType(ObjectIdentity(oid_nb_int))
    
#     try:
#         errorIndication, errorStatus, errorIndex, varBinds = next(
#             getCmd(se,
#                     cv2,
#                     t1,
#                     ContextData(),
#                     oi)
#         )

#         if errorIndication:
#             error = errorIndication
#         elif errorStatus:
#             error = f'Error in SNMP response: {errorStatus.prettyPrint()}'
#         else:
#             for varBind in varBinds:
#                 nb_int = varBind[1]
#                 print(f"nombre d'interface: {nb_int}")

#     except Exception as e:
#         error = str(e)
        
        
#     #Récupérer informations des interfaces
#     descriptions = []
#     for i in range(1, nb_int+1) :
#         index = str(i)
#         oi = ObjectType(ObjectIdentity(oid+index))
                
#         try:
#             errorIndication, errorStatus, errorIndex, varBinds = next(
#                 getCmd(se,
#                         cv2,
#                         t1,
#                         ContextData(),
#                         oi)
#             )
    
#             if errorIndication:
#                 error = errorIndication
#             elif errorStatus:
#                 error = f'Error in SNMP response: {errorStatus.prettyPrint()}'
#             else:
#                 for varBind in varBinds:
#                     # Analysez la réponse pour extraire la valeur
#                     oid_temp, value = varBind.prettyPrint().split(' = ')
#                     value = value.strip()
#                     # Créez un objet OctetString à partir de la valeur
#                     octet_string = rfc1902.OctetString(value)
                     
#                     # Pour obtenir la valeur du payload
#                     payload = octet_string.prettyPrint()
#                     descriptions.append(payload)
    
#         except Exception as e:
#             error = str(e)
#             print(error)

#     print(descriptions)
#     return nb_int, descriptions
